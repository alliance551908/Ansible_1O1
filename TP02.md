| Étape | Description |
|-------|-------------|
| **1. Cloner un référentiel GitLab** | Utilisez la commande `git clone` suivie de l'URL du référentiel GitLab que vous souhaitez cloner : `git clone https://gitlab.com/alliance551908/test.git` |
| **2. Effectuer des modifications** | Utilisez la commande `nano` (ou tout autre éditeur de texte) pour créer ou modifier des fichiers dans le référentiel cloné : `nano nom_du_fichier` |
| | Ajoutez ou modifiez le contenu du fichier selon vos besoins. |
| **3. Stager et commiter les modifications** | Utilisez la commande `git status` pour voir les fichiers modifiés et non suivis : `git status` |
| | Utilisez la commande `git add` pour ajouter les fichiers modifiés à l'index (staging area) : `git add nom_du_fichier` |
| | Utilisez la commande `git commit` pour créer un commit avec les fichiers ajoutés à l'index et ajoutez un message de commit significatif : `git commit -m "Message de commit"` |
| **4. Configurer les informations d'utilisateur Git (si nécessaire)** | Si ce n'est pas déjà fait, configurez vos informations d'utilisateur Git en utilisant les commandes suivantes : <br> `git config --global user.email "votre_email@example.com"` <br> `git config --global user.name "Votre Nom"` |
| **5. Récupérer les modifications depuis GitLab** | Utilisez la commande `git pull --no-rebase origin main` pour récupérer les modifications depuis GitLab vers votre branche locale, en particulier si des branches divergentes existent. Cette commande permet de fusionner les changements tout en conservant l'historique de chaque branche. |
| **6. Pousser les modifications vers GitLab** | Utilisez la commande `git push` suivie de l'URL du référentiel distant et de la branche sur laquelle vous souhaitez pousser les modifications : <br> `git push origin nom_de_votre_branche` |
| | Si vous êtes invité à fournir vos identifiants GitLab, entrez votre nom d'utilisateur et votre mot de passe. |
| | Une fois que la commande est exécutée avec succès, vos modifications seront poussées vers le référentiel GitLab. |
