# Ansible 1O1
```

```
# Guide facile pour la mise en place de l'infrastructure sur Microsoft Azure

Ce guide vous accompagnera étape par étape dans la mise en place d'une infrastructure de base sur Microsoft Azure, en suivant les spécifications fournies dans le cahier des charges. Assurez-vous de disposer d'un compte Azure Portal et des autorisations nécessaires pour créer des ressources.

## Cahier des charges

| Éléments                        | Détails                              |
|---------------------------------|--------------------------------------|
| Groupes de ressources           | OCC_ASD_Karim                        |
| Nom Vnet                        | Vnet_OCC_ASD_Karim                   |
| Plage Vnet                      | 10.0.13.0/24                         |
| Nom Subnet                      | Subnet_Vnet_OCC_ASD_Karim            |
| Plage Subnet                    | 10.0.13.0/28-10.0.13.15/28 (16 sous-réseaux) |
| Type de VM                      | Linux Debian 12                      |
| VM Ansible-Master               | Standard D2s v3                      |
| VM Node-web et Node-db         | Standard B1s                         |
| Connexion SSH par clé           | Utilisation d'une seule clé pour toutes les machines |

## Étape 1 : Création des ressources Azure

### 1.1 Création du groupe de ressources

1. Connectez-vous à votre compte Azure Portal.
2. Accédez à la section "Groupes de ressources".
3. Cliquez sur "Ajouter".
4. Choisissez un nom pour votre groupe de ressources, par exemple, "OCC_ASD_Karim".
5. Sélectionnez la région et cliquez sur "Review + Create".
6. Vérifiez les détails et cliquez sur "Create" pour créer le groupe de ressources.

### 1.2 Création du réseau et des sous-réseaux

1. Dans le groupe de ressources nouvellement créé, cliquez sur "Ajouter".
2. Recherchez "Réseau virtuel" et sélectionnez "Créer".
3. Choisissez un nom pour votre réseau virtuel, par exemple, "Vnet_OCC_ASD_Karim".
4. Spécifiez la plage d'adresses pour le réseau virtuel (10.0.13.0/24).
5. Ajoutez un sous-réseau en spécifiant le nom (Subnet_Vnet_OCC_ASD_Karim_Admin) et la plage d'adresses (10.0.13.0/28).
6. Répétez l'étape précédente pour créer 15 autres sous-réseaux avec les plages d'adresses fournies dans le cahier des charges.
7. Assurez-vous que chaque sous-réseau est correctement configuré avec les paramètres requis.

### 1.3 Création du groupe de sécurité

1. Dans le groupe de ressources, cliquez sur "Ajouter".
2. Recherchez "Groupe de sécurité réseau" et sélectionnez "Créer".
3. Choisissez un nom pour votre groupe de sécurité, par exemple, "NSG_OCC_ASD_Karim".
4. Configurez les règles de sécurité selon les spécifications du cahier des charges.
5. Sous l'onglet "Association du sous-réseau", ajoutez vos sous-réseaux nouvellement créés.
6. Cliquez sur "Review + Create", puis sur "Create" pour créer le groupe de sécurité.


## Étape 2 : Configuration de la VM Ansible-Master

| Éléments                        | Détails                              |
|---------------------------------|--------------------------------------|
| Création de la VM               | Dans le groupe de ressources, cliquez sur "Ajouter". Recherchez "Machine virtuelle" et sélectionnez "Créer". Remplissez les détails requis, y compris le nom, la région, le nom d'utilisateur, etc. Sous "Règles de sécurité", associez le groupe de sécurité précédemment créé. Ajoutez une règle pour autoriser le trafic SSH (port 22). Cliquez sur "Review + Create", puis sur "Create" pour créer la machine virtuelle. |
| Connexion à la VM Ansible-Master | Utilisez un terminal ou MobaXterm pour vous connecter à la VM en utilisant la commande suivante : `ssh -i "chemin_vers_votre_clé_privée.pem" azureuser@adresse_IP_publique`. Remplacez "chemin_vers_votre_clé_privée.pem" par le chemin absolu de votre clé privée. Remplacez "adresse_IP_publique" par l'adresse IP publique de votre VM. |
| Installation d'Ansible et vérification | Suivez l'une des méthodes suivantes pour installer Ansible selon vos préférences : <br> **Étape 1:** Installation via environnement virtuel <br> `sudo apt update` <br> `sudo apt install python3-venv python3-full` <br> `python3 -m venv ansible-env` <br> `source ansible-env/bin/activate` <br> `pip install ansible` <br> **Étape 2:** Créer un dossier projet <br> `mkdir ansible_quickstart && cd ansible_quickstart` <br> **Vérification du bon fonctionnement** <br> `ansible --version` <br> `ansible all -m ping -u azureuser` <br> `ansible all -m command -a "hostname"` |


## Étape 3 : Création des VM Node-web et Node-db

| Éléments                                          | Détails                                                                                                         |
|---------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
| Création des VM                                   | Répétez les étapes de création de la VM pour chaque VM, en choisissant les configurations appropriées. Assurez-vous de déployer les VM dans le même groupe de ressources et d'associer le même groupe de sécurité. |
| Connexion aux VM depuis Ansible-Master            | 1. Utilisez la commande ssh pour vous connecter à chaque VM depuis la VM Ansible-Master, en remplaçant les détails appropriés. <br> 2. Avant de vous connecter à chaque VM depuis Ansible-Master, assurez-vous de récupérer et de coller la clé privée au bon endroit pour une authentification réussie. <br> 3. Connectez-vous à la VM en utilisant la commande ssh : <br> `ssh azureuser@adresse_IP_vm` <br> 4. Une fois connecté à la VM, collez la clé privée depuis le presse-papiers dans un nouveau fichier "id_rsa" en utilisant un éditeur de texte tel que nano : <br> `nano ~/.ssh/id_rsa` <br> 5. Collez la clé privée dans le fichier et sauvegardez les modifications. <br> 6. Assurez-vous que les permissions sur le fichier "id_rsa" sont correctement définies pour des raisons de sécurité : <br> `chmod 600 ~/.ssh/id_rsa` <br> 7. Vous pouvez maintenant utiliser la clé privée "id_rsa" pour vous connecter à la VM à partir de Ansible-Master sans spécifier le chemin vers la clé privée dans la commande ssh : <br> `ssh -i ~/.ssh/id_rsa azureuser@adresse_IP_privée_vm` <br> *Assurez-vous de remplacer "azureuser" par le nom d'utilisateur sur la machine distante et "adresse_IP_vm" par l'adresse IP de la machine distante.* |
| Configuration spécifique pour Ansible             | Pour utiliser Ansible (technologie agentless), assurez-vous seulement que les éléments suivants sont configurés sur chaque machine : <br> - Accès SSH est activé. <br> - Python est installé. <br> - Le pare-feu est configuré pour permettre le trafic SSH. <br> - Les autorisations sudo sont correctement configurées. |


## Étape 4 : Mise en place de Git/GitLab

| Éléments                   | Détails                                                                                          |
|----------------------------|--------------------------------------------------------------------------------------------------|
| Installation de Git        | 1. Mettez à jour votre liste de paquets existante : `sudo apt update` <br> 2. Installez Git en utilisant apt : `sudo apt install git` <br> 3. Vérifiez l'installation de Git en vérifiant la version installée : `git --version` |
| Installation de GitLab     | GitLab peut être déployé sur votre propre infrastructure ou hébergé dans le cloud. Pour une utilisation interne, nous allons opter pour le déploiement sur votre propre infrastructure. Voici comment procéder : <br> 1. Configurez d'abord les prérequis pour GitLab. Clonez le référentiel GitLab Omnibus : `git clone https://gitlab.com/gitlab-org/omnibus-gitlab.git` <br> 3. Accédez au répertoire cloné : `cd omnibus-gitlab` <br> 4. Vérifiez la dernière version disponible : `git tag -l | grep '^[0-9]\+\(\.[0-9]\+\)*$' | sort -V` <br> 5. Sélectionnez la dernière version <br> 8. Une fois le déploiement terminé, vous pourrez accéder à GitLab via votre navigateur en utilisant l'adresse IP ou le nom de domaine de votre serveur. |


## Étape 5 : Provisioning des machines node

https://docs.google.com/document/d/1M2A7gjyY6UfHFbnoq36I-tFZNeVNdele6t64iG5hZdU/edit?usp=sharing